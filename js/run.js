/* 
 * felipebevi - teste actualsales - 20160725
 */

function run(action,params,callback){
	var _params = params!=''?params:{};
	var retorno=false;
	$.ajax({
	  method: "POST",
	  url: "run.php",
	  data: {action:action,param:_params},
	  async: false
	})
	.always(function( ret ) {
            if(ret.responseText!=undefined){
                ret = ret.responseText;
            }
	    if(callback!=undefined){
	    	callback(ret);
	    }else{	    
	    	retorno = ret;
	    }
	});	  
	return retorno;  
}