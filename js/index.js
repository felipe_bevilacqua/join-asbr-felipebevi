/* 
 * felipebevi - teste actualsales - 20160725
 */


function validar(){
    var nome            = $('input[name="nome"]').val().trim();
    var data_nascimento = $('input[name="data_nascimento"]').val();
    var email           = $('input[name="email"]').val().trim();
    var telefone        = $('input[name="telefone"]').val().trim();
    
    var RegExpression = /[0-9_~\=\-!@#\$%\^&\*\(\)]+$/;
        
    if(
        nome.split(' ').length < 2 ||
        RegExpression.test(nome)
    ){
        alerta("No mínimo duas palavras contendo apenas caracteres de A a Z e suas possíveis acentuações.","Atenção",function(){ $('input[name="nome"]').focus(); });
        return false;
    }
    
    if(data_nascimento=='' || data_nascimento.length!=10 || parseDMY(data_nascimento) > new Date){
        alerta("Insira uma data de nascimento válida, menor do que a atual.","Atenção",function(){ $('input[name="data_nascimento"]').focus(); });
        return false;
    }
    
    if(email==''){
        alerta("Insira um e-mail.","Atenção",function(){ $('input[name="email"]').focus(); });
        return false;
    }
    
    if(telefone==''){
        alerta("Insira um telefone.","Atenção",function(){ $('input[name="telefone"]').focus(); });
        return false;
    }
    
    return true;
}

function alerta(msg,titulo,callback){
    $('#myModal h4.modal-title').html(titulo);
    $('#myModal div.modal-body').html(msg);
    $('#myModal').modal('show');
    if(callback != undefined && typeof callback=='function'){
        dismissModal(callback);
    }
}

function dismissModal(callback){
    $('#myModal').on('hidden.bs.modal', function (e) {
        callback();
    });
}

function parseDMY(value) {
    var date = value.split("/");
    var d = parseInt(date[0], 10),
        m = parseInt(date[1], 10),
        y = parseInt(date[2], 10);
    return new Date(y, m - 1, d);
}

function idadePreenchida(){
    
    if($('input[name="data_nascimento"]').val()==''){
        return false;
    }
    
    // ********
    // ATENÇÃO: Para o cálculo da idade, considerar a data atual fixa em 01/06/2016
    // ********
    // Para usar cálculo real, descomentar a linha abaixo e comentar a consequente
    //var diferencaMilisegundos = Date.now() - parseDMY($('input[name="data_nascimento"]').val());
    var diferencaMilisegundos = parseDMY('01/06/2016') - parseDMY($('input[name="data_nascimento"]').val());    
    var idadeData = new Date(diferencaMilisegundos);
    return Math.abs(idadeData.getUTCFullYear() - 1970);    
}

function getPontosPreenchidos(){
    /*
     *  Sul: -2 pontos
     *  Sudeste: -1 ponto, exceto quando unidade = São Paulo (que não modifica)
     *  Centro-Oeste: -3 pontos
     *  Nordeste: -4 pontos
     *  Norte: -5 pontos
     * 
     *  A partir de 100 ou menor que 18: -5 pontos
     *  Entre 40 e 99: -3 pontos
     *  Entre 18 e 39: não modifica
     */
    var pontos       = 10;
    var pontosRegiao = $('select[name="regiao"]').find(':selected').attr('pontos');
    var pontosCidade = $('select[name="unidade"]').find(':selected').attr('pontos');
    var idade  = idadePreenchida();
    if(idade){
        var pontuacaoIdade = { 0:-5,18:0,40:-3,100:-5 };
        var pontosIdade = 0;
        for(var i in pontuacaoIdade){
            if(idade>=i){
                pontosIdade = pontuacaoIdade[i];
            }
        }
        pontos = parseInt(pontos)+parseInt(pontosRegiao)+parseInt(pontosCidade)+parseInt(pontosIdade);
        return pontos;
    }else{
        return 0;
    }
}

function preencheRegioes(){
    var select = $('select[name="regiao"]');
    select.find('option').remove();
    for(var i in regioes){
        select.append($('<option>').val(regioes[i].id).attr('pontos',regioes[i].pontos).text(regioes[i].nome));
    }
}

function preencheCidades(regiaoId){
    var select = $('select[name="unidade"]');
    select.find('option').remove();
    for(var i in cidades[regiaoId]){
        if(typeof cidades[regiaoId][i]!=undefined){
            cidades[regiaoId][i].pontos = cidades[regiaoId][i].pontos==undefined?0:cidades[regiaoId][i].pontos;
            select.append($('<option>').attr('pontos',cidades[regiaoId][i].pontos).val(cidades[regiaoId][i].id).text(cidades[regiaoId][i].nome));
        }
    }
    if(!select.find('option').length){
        select.append($('<option>').attr('pontos',0).val(0).text('INDISPONÍVEL'));
    }
}

function getToken(){
    var tokenGerado = '85fcbb8dda2d9c573213bd88b9b6f153'; // http://api.actualsales.com.br/join-asbr/ti/token?email=felipe@9ideias.com // em 20160726
    return tokenGerado;
}

function registraLead(){
    var nome            = $('input[name="nome"]').val(); // (String)
    var email           = $('input[name="email"]').val(); // (String)
    var telefone        = $('input[name="telefone"]').val(); // (String)
    var regiao          = $('select[name="regiao"]').find(':selected').text(); // (Elemento do conjunto ["Norte", "Nordeste", "Sul", "Sudeste", "Centro-Oeste"])
    var unidade         = $('select[name="unidade"]').find(':selected').text(); // (Elemento do conjunto ["Porto Alegre", "Curitiba", "São Paulo", "Rio de Janeiro", "Belo Horizonte", "Brasília", "Salvador", "Recife", "INDISPONÍVEL"])
    var data_nascimento = $('input[name="data_nascimento"]').val().split('/').reverse().join('-'); // (data no formato YYYY-mm-dd)
    var score           = getPontosPreenchidos(); // (int de 0 a 10)
    var token           = getToken(); // (String)
    var objPost = {nome:nome,email:email,telefone:telefone,regiao:regiao,unidade:unidade,data_nascimento:data_nascimento,score:score,token:token};

    // enviar para http://api.actualsales.com.br/join-asbr/ti/lead - via POST
    $.ajax({
      method: "POST",
      url: "http://api.actualsales.com.br/join-asbr/ti/lead",
      data: objPost,
      async: false
    });
    
    // grava no banco local
    run('gravaLead',objPost);

    return true;
}

var regioes = {};
var cidades = {};

$(function () {
    // alimenta global de regioes
    var temp_regioes = run('getRegioes',{});
    for(var i in temp_regioes){
        regioes[temp_regioes[i].id] = temp_regioes[i];
    }
    // alimenta global de cidades
    var temp_cidades = run('getCidades',{});
    for(var i in temp_cidades){
        if(cidades[temp_cidades[i].id_regiao]==undefined){
            cidades[temp_cidades[i].id_regiao] = {};
        }
        cidades[temp_cidades[i].id_regiao][temp_cidades[i].id] = temp_cidades[i];
    }
    // cria mascaras de conteudo para o STEP1
    $('input[name="data_nascimento"]').mask('00/00/0000');
    $('input[name="email"]').mask("A", {
	translation: {
		"A": { pattern: /[\w@\-.+]/, recursive: true }
	}
    });
    // cria mascaras de conteudo para telefone de SP
    var SPMaskBehavior = function (val) {
        return val.replace(/\D/g, '').length === 11 ? '(00) 00000-0000' : '(00) 0000-00009';
    },
    spOptions = {
        onKeyPress: function(val, e, field, options) {
            field.mask(SPMaskBehavior.apply({}, arguments), options);
          }
    };
    $('input[name="telefone"]').mask(SPMaskBehavior, spOptions);
    // preenche o combo de regioes e cria o trigger para preencher o combo de cidades automaticamente
    preencheRegioes();
    $('select[name="regiao"]').unbind('change').bind('change',function(){
        preencheCidades($(this).find(':selected').val());
    }).trigger('change');
    // define acao para novo botao de voltar 1 STEP
    $('.last-step').click(function () {
        if(registraLead()){
            $(this).parents('.form-step').hide().next().show();
        }
    });
});