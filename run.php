<?php

/* 
 * felipebevi - teste actualsales - 20160725
 * versao ultra-resumida de programacao orientada a EVENTOS - utilizando sistema de RUNCALL dinamico de acordo com classe/metodo que foi reduzida apenas para metodos dentro de apenas 1 classe (DB)
 */

class DB{
    public $conn;
    public function __construct() {        
        if($this->conn==NULL){            
            $conn = mysql_connect('localhost','asbr','asbr');
            mysql_select_db('asbr');
            mysql_set_charset("UTF8");
            $this->conn = $conn;
        }     
    }
    public function query($sql){
        return mysql_query($sql);
    }
    public function num_rows($res){
        return mysql_num_rows($res);
    }
    public function fetch($res){
        return mysql_fetch_assoc($res);
    }
    
    private function getRegistros($query){
        $res = $this->query($query);
        $registros = array();
        if($this->num_rows($res)){
            while($ret = $this->fetch($res)){
                $registros[]=$ret;
            }
        }
        return $registros;
    }
    
    public function getRegioes($param){
        return $this->getRegistros("select * from regioes order by nome");
    }
    
    // ao inves de filtrar no banco por regiao, levo todos pro frontend para ser trocado em tempo real sem processar no banco toda hora - medida de acordo com cada solucao
    public function getCidades($param){
        return $this->getRegistros("select * from cidades order by nome");
    }
    
    public function gravaLead($param){
        $SQL = <<<SQL
                INSERT INTO leads(id, data_hora, nome, email, telefone, regiao, unidade, data_nascimento, score, token) 
                    VALUES (
                                0,
                                now(),
                                '{$param['nome']}',
                                '{$param['email']}',
                                '{$param['telefone']}',
                                '{$param['regiao']}',
                                '{$param['unidade']}',
                                '{$param['data_nascimento']}',
                                {$param['score']},
                                '{$param['token']}'
                          )
SQL;
        $this->query($SQL);
    }
}

$action     = $_REQUEST['action'];
$param      = $_REQUEST['param'];
$db         = new DB();
if(method_exists($db,$action)){
    $retorno = $db->$action($param);
    $retorno = is_array($retorno) && count($retorno)>0?$retorno:array();
    header('Content-Type: application/json');
    echo json_encode($retorno,1);
}