-- phpMyAdmin SQL Dump
-- version 4.6.3
-- https://www.phpmyadmin.net/
--
-- Host: localhost:3306
-- Generation Time: 27-Jul-2016 às 00:23
-- Versão do servidor: 5.7.13
-- PHP Version: 5.5.36

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `asbr`
--

-- --------------------------------------------------------

--
-- Estrutura da tabela `cidades`
--

CREATE TABLE `cidades` (
  `id` int(11) NOT NULL,
  `id_regiao` int(11) NOT NULL,
  `nome` varchar(50) NOT NULL,
  `pontos` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Extraindo dados da tabela `cidades`
--

INSERT INTO `cidades` (`id`, `id_regiao`, `nome`, `pontos`) VALUES
(1, 1, 'Porto Alegre', NULL),
(2, 1, 'Curitiba', NULL),
(3, 2, 'São Paulo', 1),
(4, 2, 'Rio de Janeiro', NULL),
(5, 2, 'Belo Horizonte', NULL),
(6, 3, 'Brasília', NULL),
(7, 4, 'Salvador', NULL),
(8, 4, 'Recife', NULL);

-- --------------------------------------------------------

--
-- Estrutura da tabela `leads`
--

CREATE TABLE `leads` (
  `id` int(11) NOT NULL,
  `data_hora` datetime NOT NULL,
  `nome` varchar(100) NOT NULL,
  `email` varchar(100) NOT NULL,
  `telefone` varchar(50) NOT NULL,
  `regiao` varchar(50) NOT NULL,
  `unidade` varchar(50) NOT NULL,
  `data_nascimento` date NOT NULL,
  `score` int(11) NOT NULL,
  `token` varchar(50) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Extraindo dados da tabela `leads`
--

INSERT INTO `leads` (`id`, `data_hora`, `nome`, `email`, `telefone`, `regiao`, `unidade`, `data_nascimento`, `score`, `token`) VALUES
(1, '2016-07-26 21:11:45', 'Felipe Bevilacqua', 'felipe@bevinet.com', '(11) 2206-1220', 'Sul', 'Porto Alegre', '1987-07-10', 8, '85fcbb8dda2d9c573213bd88b9b6f153'),
(2, '2016-07-26 21:22:02', 'Felipe Bevilacqua', 'felipe@9ideias.com', '(11) 97600-4400', 'Sudeste', 'São Paulo', '1987-07-10', 10, '85fcbb8dda2d9c573213bd88b9b6f153');

-- --------------------------------------------------------

--
-- Estrutura da tabela `regioes`
--

CREATE TABLE `regioes` (
  `id` int(11) NOT NULL,
  `nome` varchar(50) NOT NULL,
  `pontos` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Extraindo dados da tabela `regioes`
--

INSERT INTO `regioes` (`id`, `nome`, `pontos`) VALUES
(1, 'Sul', -2),
(2, 'Sudeste', -1),
(3, 'Centro-Oeste', -3),
(4, 'Nordeste', -4),
(5, 'Norte', -5);

--
-- Indexes for dumped tables
--

--
-- Indexes for table `cidades`
--
ALTER TABLE `cidades`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `leads`
--
ALTER TABLE `leads`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `regioes`
--
ALTER TABLE `regioes`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `cidades`
--
ALTER TABLE `cidades`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;
--
-- AUTO_INCREMENT for table `leads`
--
ALTER TABLE `leads`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT for table `regioes`
--
ALTER TABLE `regioes`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
