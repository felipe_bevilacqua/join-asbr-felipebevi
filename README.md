# Teste finalizado - Felipe Bevilacqua - opinião pessoal:
Adquirindo conhecimento e analisando os outros códigos deste teste no bitbucket (buscando por join-asbr), percebi que TODOS, todos os outros participantes criam enormes MONSTROS de códigos para uma LANDING-PAGE... cara é "só uma landing-page"... ela precisa ser sim segura, validada, mas, SIMPLES.  Imagina se pra cada cliente que quiser uma simples landing-page voce adicionar um framework (que também é do gosto de cada um) para ela!!?? um coloca laravel, outro angular, outro codeigniter, meu, é tudo PHP!... Adiciona um ajax em javascript/jquery mesmo e fim!... tem que ser simples e funcional para TODOS, não só pra quem fez, mas para quem vai dar manutenção, quem vai avaliar, etc. 

Essa é a minha opinião.  Me coloco no lugar do cara que vai alterar esses códigos gigantes, com CENTENAS de arquivos... vou conseguir? SIM.  Vou demorar? MUITO...  É triste ver que hoje em dia, programador (seja em qualquer linguagem) seja direcionado para esta forma de trabalho, enfiar um framework à gosto na cabeça e tentar meter ele em qualquer solução... fazendo do simples virar um monstro só pra parecer com o que ele conhece...  

PHP, é esta a solução solicitada.  Sem mistério!... valida o que o cara escreveu, posta pro PHP, grava no banco e replica pra API do asbr.  Só isso! :-) (OBS: minha solucao demandou apenas 333 linhas codificadas e identadas!)

A meu ver é isso, estou aberto a críticas e sugestões, talvez só eu esteja viajando em fazer as coisas mais puramente simples e funcionais sem ser o foco do solicitado, ou que não tenha ficado claro qual era o real objetivo pra mim.

É isso aí, um abraço, Felipe - felipe@9ideias.com



-------------------------------------------------------------------
# Venha trabalhar na ActualSales!

## A Empresa
A ActualSales é uma empresa de marketing digital focada em performance.

## Vagas

### Developer Full Stack
Estamos procurando um developer interessado em dar continuidade  ao desenvolvimento e evolução de um projeto desafiador e ganhador de dois prêmios ABEMD Awards.
Trata-se de uma plataforma de aquisição de **Cartões de Crédito Online**.

#### Requisitos:
- Experiência de no mínimo 4 anos em Desenvolvimento Web (PHP)
- Conseguir se virar em qualquer situação que surja (no escopo de TI)
- Facilidade para lidar com clientes
- Facilidade em trabalhar sobre pressão
- Conhecimento e vontade (principalmente) de aplicar boas práticas e metodologias
- Sentir-se à vontade para ajudar e orientar os demais membros da sua equipe

#### Bônus:
- Interesse por infra e ops

#### Nosso stack atual:
- PHP (framework baseado no Zend)
- MySQL
- Git (Bitbucket com deploy contínuo)
- jQuery
- Bootstrap
- Apache
- Varnish
- Importante ressaltar que esse stack está sempre aberto para novas tecnologias

#### Projetos:
- Landing Pages
- Plataformas de eCommerce
- CRM
- Aplicativos Mobile e Facebook

#### Oferecemos:
- Ambiente informal
- Comissão Mensal sobre os resultados
- 15 Salários Anuais

#### Como me candidatar?
Não queremos ver o seu CV, mas sim o seu código.
Para isso, elaboramos um pequeno teste de aptidões que pode ser realizado no seu tempo.
As instruções para execução desse teste estão localizadas na pasta landing-page desse mesmo repositório.

#### Dúvidas
Para dúvidas ou mais informações, fique à vontade para nos contactar através do email <fernando.canteiro@actualsales.com.br>.


Obrigado e boa sorte!